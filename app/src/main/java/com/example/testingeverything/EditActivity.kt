package com.example.testingeverything


import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_edit.*

class EditActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit)
        getPerson(person)

    }
    fun getPerson(Parent:Parent) {
        nameTextView.text = Parent.name.toString()
        lastNameTextView.text = Parent.lastName.toString()
        ageTextView.text = Parent.age.toString()
        var lang: String = ""
        for (element in Parent.language)
            lang += "$element "
        languageTextView.text = lang
    }

}






